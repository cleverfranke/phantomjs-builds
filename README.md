This repository contains binary builds for PhantomJS on architectures we commonly use and binary
builds are not created for, as the building takes a lot of time.

For more info on phantomjs: http://phantomjs.org/
For more info on building: http://phantomjs.org/build.html
